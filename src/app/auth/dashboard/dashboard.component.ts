import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../shared/services/auth.service";
import { ContactServiceService } from "../../shared/services/contact-service.service";

import { HttpClient } from "@angular/common/http";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  profileForm = new FormGroup({
    name: new FormControl(""),
    phone: new FormControl(""),
    // category: new FormControl(""),
    message: new FormControl("")
  });
  constructor(
    private httpClient: HttpClient,
    public authService: AuthService,
    public contactService: ContactServiceService
  ) {}

  onSubmit() {
    console.log(this.profileForm.value);
    this.contactService.sendContact(this.profileForm.value).subscribe(() => {
      console.log("ca marche");
    });
  }

  ngOnInit() {}
}
