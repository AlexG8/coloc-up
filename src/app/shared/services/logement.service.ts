import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Rent } from "../../interface/rent";
import { User } from "../../interface/user";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from "firebase";

@Injectable({
  providedIn: "root"
})
export class LogementService {
  rents: Observable<Rent[]>;
  rentCollection: any;
  userCollection: any;
  users: Observable<User[]>;

  constructor(public afs: AngularFirestore, private router: Router) {}

  createRent(rent: Rent) {
    return new Promise<any>((resolve, reject) => {
      this.afs
        .collection("rents")
        .add(rent)
        .then(
          res => {
            this.afs
              .collection("rents")
              .doc(res.id)
              .update({
                id: res.id
              })
              .then(() => {
                console.log("ID ajouter", res.id);
                this.router.navigate(["/"]);
              });
            console.log(res.id);
            resolve(res);
          },
          err => reject(err)
        );
    });
  }

  getRents() {
    this.rentCollection = this.afs.collection("rents");
    this.rents = this.rentCollection.valueChanges();

    return this.rents;
  }

  getUsers() {
    this.userCollection = this.afs.collection("users");
    this.users = this.userCollection.valueChanges();

    return this.users;
  }

  getRentId(id: string) {
    return new Promise<any>((resolve, reject) => {
      this.afs
        .collection("rents")
        .doc(id)
        .ref.get()
        .then(doc => {
          if (doc) {
            resolve(doc.data());
          } else {
            console.log("No such document!");
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  deleteRent(id: string) {
    console.log("arrive dans service");
    return this.afs.doc("/rents/" + id).delete();
  }

  deleteUser(id: string) {
    console.log("arrive dans service");
    return this.afs.doc("/users/" + id).delete();
  }

  updateRent(id: string, formData) {
    this.afs
      .collection("rents")
      .doc(id)
      .update({
        title: formData.title,
        town: formData.town,
        department: formData.department,
        nbBedroom: formData.nbBedroom,
        superficie: formData.superficie,
        animaux: formData.animaux,
        wifi: formData.wifi,
        nblit: formData.nblit,
        address: formData.address,
        startDate: formData.startDate,
        endDate: formData.endDate,
        image: formData.image,
        image2: formData.image2,
        image3: formData.image3,
        image4: formData.image4,
        price: formData.price,
        description: formData.description
      });
  }

  /*   uploadImageRent(file: File) {
    return new Promise((resolve, reject) => {
      const uniqueId = Date.now().toString();
      const fileName = uniqueId + file.name;
      const upload = firebase
        .storage()
        .ref()
        .child("images/rent" + fileName)
        .put(file);

      upload.on(
        firebase.storage.TaskEvent.STATE_CHANGED,
        () => {
          console.log("Chargement");
        },
        error => {
          console.log(error);
          reject(error);
        },
        () => {
          upload.snapshot.ref.getDownloadURL().then(downloadUrl => {
            resolve(downloadUrl);
          });
        }
      );
    });
  } */
}
