import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ContactServiceService {
  adressAPI = "http://localhost:3000/send-email";

  constructor(private httpClient: HttpClient) {}
  sendContact(body) {
    console.log("service");
    return this.httpClient.post(this.adressAPI, body);
  }
}
