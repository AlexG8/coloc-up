import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ApiService {
  adressAPI = "http://localhost:3000/send-email";
  constructor(private httpClient: HttpClient) {
    // this.favorites = this.db.list("favoritesCart").valueChanges();
  }
  sendContact(body) {
    console.log("service");
    return this.httpClient.post(this.adressAPI, body);
  }
}
