export interface Rent {
  title?: String;
  town?: String;
  id?: String;
  department?: Number;
  nbBedroom?: Number;
  superficie?: Number;
  animaux?: String;
  wifi?: String;
  nblit?: Number;
  address?: String;
  startDate?: Date;
  endDate?: Date;
  image?: String;
  image2?: String;
  image3?: String;
  image4?: String;
  /*photo?: String;*/
  firstname?: String;
  lastname?: String;
  photoProfile?: String;
  email?: String;
  phone?: Number;
  price?: String;
  description?: String;
}
