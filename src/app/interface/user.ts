export interface User {
  displayName: String;
  email: String;
  emailVerified: Boolean;
  firstname: String;
  lastname: String;
  photoURL: String;
  role: String;
  uid: String;
}
