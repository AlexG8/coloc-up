import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { NavComponent } from "./header/nav/nav.component";
import { FooterComponent } from "./footer/footer.component";
import { BackgroundHeaderComponent } from "./header/background-header/background-header.component";
import { HomepageComponent } from "./homepage/homepage.component";
import { DashboardComponent } from "./auth/dashboard/dashboard.component";
import { LoginComponent } from "./auth/login/login.component";
import { RegisterComponent } from "./auth/register/register.component";
import { ForgotPasswordComponent } from "./auth/forgot-password/forgot-password.component";
import { VerifyEmailComponent } from "./auth/verify-email/verify-email.component";
import { DashboardAdminComponent } from "./dashboard-admin/dashboard-admin.component";
import { HttpClient, HttpClientModule } from "@angular/common/http";

// import form
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

// Auth service
import { AuthService } from "./shared/services/auth.service";

// Firebase services + enviorment module
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "../environments/environment";
import { PostAnnonceComponent } from "./colocation/post-annonce/post-annonce.component";
import { ReadAnnonceComponent } from "./colocation/read-annonce/read-annonce.component";
import { Part1Component } from "./main/part1/part1.component";
import { SinglerentComponent } from "./colocation/singlerent/singlerent.component";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ContactComponent } from "./contact/contact.component";

import { MaterialModule } from "./material-ui.module";

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BackgroundHeaderComponent,
    HomepageComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    PostAnnonceComponent,
    ReadAnnonceComponent,
    Part1Component,
    SinglerentComponent,
    FooterComponent,
    DashboardAdminComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule {}
