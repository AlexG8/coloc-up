import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { AppComponent } from "./app.component";
import { HomepageComponent } from "./homepage/homepage.component";

// Import canActivate guard services
import { AuthGuard } from "./shared/guard/auth.guard";
import { SecureInnerPagesGuard } from "./shared/guard/secure-inner-pages.guard.ts.guard";

// Import all the components for which navigation service has to be activated
import { LoginComponent } from "./auth/login/login.component";
import { RegisterComponent } from "./auth/register/register.component";
import { DashboardComponent } from "./auth/dashboard/dashboard.component";
import { ForgotPasswordComponent } from "./auth/forgot-password/forgot-password.component";
import { VerifyEmailComponent } from "./auth/verify-email/verify-email.component";
import { PostAnnonceComponent } from "./colocation/post-annonce/post-annonce.component";
import { ReadAnnonceComponent } from "./colocation/read-annonce/read-annonce.component";
import { SinglerentComponent } from "./colocation/singlerent/singlerent.component";
import { DashboardAdminComponent } from "./dashboard-admin/dashboard-admin.component";
import { AdminGuard } from "./shared/guard/admin.guard";

const routes: Routes = [
  { path: "", component: HomepageComponent },
  {
    path: "connexion",
    component: LoginComponent,
    canActivate: [SecureInnerPagesGuard]
  },
  {
    path: "inscription",
    component: RegisterComponent,
    canActivate: [SecureInnerPagesGuard]
  },
  { path: "profile", component: DashboardComponent, canActivate: [AuthGuard] },
  {
    path: "reset-pass",
    component: ForgotPasswordComponent,
    canActivate: [SecureInnerPagesGuard]
  },
  {
    path: "verifier-email",
    component: VerifyEmailComponent,
    canActivate: [SecureInnerPagesGuard]
  },
  {
    path: "publier-annonce",
    component: PostAnnonceComponent,
    canActivate: [AuthGuard]
  },
  { path: "test", component: ReadAnnonceComponent },
  { path: "rent/:id", component: SinglerentComponent },
  {
    path: "admin-dashboard",
    component: DashboardAdminComponent,
    canActivate: [AdminGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: "enabled"
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
