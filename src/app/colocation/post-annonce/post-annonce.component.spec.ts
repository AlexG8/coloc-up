import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostAnnonceComponent } from './post-annonce.component';

describe('PostAnnonceComponent', () => {
  let component: PostAnnonceComponent;
  let fixture: ComponentFixture<PostAnnonceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostAnnonceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
