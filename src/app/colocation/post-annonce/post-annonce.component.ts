import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { LogementService } from "src/app/shared/services/logement.service";

@Component({
  selector: "app-post-annonce",
  templateUrl: "./post-annonce.component.html",
  styleUrls: ["./post-annonce.component.css"]
})
export class PostAnnonceComponent implements OnInit {
  rentForm: FormGroup;

  /*   photoUploading = false;
  photoUploaded = false;
  photoUrl: string; */

  constructor(
    private fb: FormBuilder,
    private logementService: LogementService
  ) {}

  ngOnInit() {
    this.initRentForm();
  }

  initRentForm() {
    this.rentForm = this.fb.group({
      title: ["", Validators.required],
      town: ["", Validators.required],
      department: ["", Validators.required],
      nbBedroom: ["", Validators.required],
      address: ["", Validators.required],
      startDate: ["", Validators.required],
      endDate: ["", Validators.required],
      image: ["", Validators.required],
      image2: ["", Validators.required],
      image3: ["", Validators.required],
      image4: ["", Validators.required],
      price: ["", Validators.required],
      superficie: ["", Validators.required],
      animaux: ["", Validators.required],
      wifi: ["", Validators.required],
      nblit: ["", Validators.required],
      description: ["", Validators.required],
      email: ["", Validators.required],
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      gender: ["", Validators.required],
      phone: ["", Validators.required],
      photoProfile: ""
    });
  }

  onSubmitRentForm() {
    const rentValue = this.rentForm.value;
    this.logementService.createRent(rentValue);
  }

  /*onUploadFile(event) {
    this.photoUploading = true;
    console.log(event);
    this.logementService
      .uploadImageRent(event.target.files[0])
      .then((url: string) => {
        this.photoUrl = url;
        this.photoUploading = true;
        this.photoUploaded = true;
        setTimeout(() => {
          this.photoUploaded = false;
        }, 5000);
      });
  }*/
}
