import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ReadAnnonceComponent } from "./read-annonce.component";

describe("ReadAnnonceComponent", () => {
  let component: ReadAnnonceComponent;
  let fixture: ComponentFixture<ReadAnnonceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReadAnnonceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReadAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
