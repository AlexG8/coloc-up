import { Component, OnInit } from "@angular/core";
import { LogementService } from "src/app/shared/services/logement.service";
import { Rent } from "../../interface/rent";
import { Router } from "@angular/router";

@Component({
  selector: "app-read-annonce",
  templateUrl: "./read-annonce.component.html",
  styleUrls: ["./read-annonce.component.css"]
})
export class ReadAnnonceComponent implements OnInit {
  rents: Rent[];

  
  constructor(
    private logementService: LogementService,
    private router: Router
  ) {}

  ngOnInit() {
    this.logementService.getRents().subscribe(rents => {
      console.log(rents);
      this.rents = rents;
    });
  }

  accesRent(id: any) {
    this.router.navigate(["/rent", id]);
  }
}
