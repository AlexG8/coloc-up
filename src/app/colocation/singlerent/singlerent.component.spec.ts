import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinglerentComponent } from './singlerent.component';

describe('SinglerentComponent', () => {
  let component: SinglerentComponent;
  let fixture: ComponentFixture<SinglerentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinglerentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinglerentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
