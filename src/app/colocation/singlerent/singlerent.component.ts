import { Component, OnInit } from "@angular/core";
import { LogementService } from "src/app/shared/services/logement.service";
import { AuthService } from "../../shared/services/auth.service";
import { Rent } from "../../interface/rent";
import { Router, ActivatedRoute } from "@angular/router";
import { trigger, transition, style, animate } from "@angular/animations";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@Component({
  selector: "app-singlerent",
  templateUrl: "./singlerent.component.html",
  styleUrls: ["./singlerent.component.css"],
  animations: [
    trigger("inGoAnimation", [
      transition(":enter", [
        style({ transform: "translateX(100%)", opacity: 0 }),
        animate("500ms", style({ transform: "translateX(0)", opacity: 1 }))
      ])
    ])
  ]
})
export class SinglerentComponent implements OnInit {
  rents: Rent[];

  logements: Rent = {
    title: "",
    town: "",
    department: 0,
    nbBedroom: 0,
    address: "",
    image: "",
    price: "",
    description: ""
  };

  constructor(
    private logementService: LogementService,
    private router: ActivatedRoute,
    public authService: AuthService
  ) {}

  ngOnInit() {
    const id = this.router.snapshot.paramMap.get("id");
    console.log(id);

    this.logementService
      .getRentId(id)
      .then((logements: Rent) => {
        this.logements = logements;
        console.log(this.logements);
      })
      .catch(err => {
        console.log(err);
      });
  }

  //slider
  isShow = true;
  isShow2 = false;
  isShow3 = false;
  isShow4 = false;
  goPhoto2() {
    this.isShow = false;
    this.isShow2 = true;
    this.isShow3 = false;
    this.isShow4 = false;
  }
  goPhoto3() {
    this.isShow = false;
    this.isShow2 = false;
    this.isShow3 = true;
    this.isShow4 = false;
  }
  goPhoto4() {
    this.isShow = false;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = true;
  }
  goPhoto() {
    this.isShow = true;
    this.isShow2 = false;
    this.isShow3 = false;
    this.isShow4 = false;
  }
}
