import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/shared/services/auth.service";
import * as firebase from "firebase";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.css"]
})
export class NavComponent implements OnInit {
  isLoggedIn = false;
  isAdmin = false;

  constructor(private AuthService: AuthService) {}

  ngOnInit() {
    firebase.auth().onAuthStateChanged(userSession => {
      console.log(userSession);
      this.isLoggedIn = this.AuthService.isLoggedIn;
      this.isAdmin = this.AuthService.isAdmin;
    });
  }

  logout() {
    this.AuthService.SignOut();
  }
}
