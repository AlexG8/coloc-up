import { Component, OnInit } from "@angular/core";
import * as $ from "jquery";

@Component({
  selector: "app-background-header",
  templateUrl: "./background-header.component.html",
  styleUrls: ["./background-header.component.css"]
})
export class BackgroundHeaderComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    $("#test").on("click", function() {
      /**
       * Smooth scrolling to a specific element
       **/
      function goDown(target: JQuery<HTMLElement>) {
        if (target.length) {
          $("html, body")
            .stop()
            .animate({ scrollTop: target.offset().top }, 900);
        }
      }
      // exemple
      goDown($("#goici"));
    });
  }
}
