import { ApiService } from "../shared/api.service";
import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { HttpClient } from "@angular/common/http";
import { FormGroup, FormControl } from "@angular/forms";

@Component({
  selector: "app-contact",
  templateUrl: "./contact.component.html",
  styleUrls: ["./contact.component.css"]
})
export class ContactComponent implements OnInit {
  profileForm = new FormGroup({
    name: new FormControl(""),
    phone: new FormControl(""),
    // category: new FormControl(""),
    message: new FormControl("")
  });
  constructor(
    private httpClient: HttpClient,
    private contactService: ApiService
  ) {}

  onSubmit() {
    console.log(this.profileForm.value);
    this.contactService.sendContact(this.profileForm.value).subscribe(() => {
      console.log("ca marche");
    });
  }
  ngOnInit() {}
}
