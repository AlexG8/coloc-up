import { Component, OnInit } from "@angular/core";
import { LogementService } from "../shared/services/logement.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { User } from "../interface/user";
import { Rent } from "../../app/interface/rent";
import * as firebase from "firebase";

@Component({
  selector: "app-dashboard-admin",
  templateUrl: "./dashboard-admin.component.html",
  styleUrls: ["./dashboard-admin.component.css"]
})
export class DashboardAdminComponent implements OnInit {
  users: User[];
  rents: Rent[];
  formLogement: FormGroup;

  constructor(
    private logementService: LogementService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    // Get logement from service
    this.logementService.getRents().subscribe(rents => {
      this.rents = rents;
      console.log(this.rents, "rents");
    });

    // initialize le reactiveform pour les logements
    this.createFormLogement();

    // TODO User get & Form
    this.logementService.getUsers().subscribe(users => {
      this.users = users;
      console.log(this.users, "users");
    });
  }
  isShow = true;
  isShow2 = false;

  createFormLogement() {
    this.formLogement = this.fb.group({
      title: ["", Validators.required],
      town: ["", Validators.required],
      department: ["", Validators.required],
      nbBedroom: ["", Validators.required],
      address: ["", Validators.required],
      startDate: ["", Validators.required],
      endDate: ["", Validators.required],
      image: ["", Validators.required],
      image2: ["", Validators.required],
      image3: ["", Validators.required],
      image4: ["", Validators.required],
      price: ["", Validators.required],
      superficie: ["", Validators.required],
      animaux: ["", Validators.required],
      wifi: ["", Validators.required],
      nblit: ["", Validators.required],
      description: ["", Validators.required]
    });
  }

  onSubmitRentForm() {
    const rentValue = this.formLogement.value;
    this.logementService.createRent(rentValue);
    console.log(this.rents);
  }

  deleteLogement(id) {
    if (window.confirm("Voulez vous vraiment supprimer ce logement"))
      this.logementService.deleteRent(id);
    console.log(id);
  }

  deleteUser(id) {
    if (window.confirm("Voulez vous vraiment supprimer cet utilisateur"))
      this.logementService.deleteUser(id);
    console.log(id);
  }

  updateLogement(id) {
    console.log(id);
    console.log(this.formLogement.value);
    this.logementService.updateRent(id, this.formLogement.value);
  }

  goLogement() {
    // this.isShow = !this.isShow;
    this.isShow = true;
    this.isShow2 = false;
  }
  goUser() {
    // this.isShow = !this.isShow;
    this.isShow2 = true;
    this.isShow = false;
  }

  // TODO delete/update user
}
