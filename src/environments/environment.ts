// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyAKY8Xl4ItGntVvCkNm2AtEa0ieQQzG2WQ",
    authDomain: "coloc-up.firebaseapp.com",
    databaseURL: "https://coloc-up.firebaseio.com",
    projectId: "coloc-up",
    storageBucket: "coloc-up.appspot.com",
    messagingSenderId: "31333711814",
    appId: "1:31333711814:web:4823c476acdc6b64c8fcc7",
    measurementId: "G-0P0KP9D7SY"
  }

};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
